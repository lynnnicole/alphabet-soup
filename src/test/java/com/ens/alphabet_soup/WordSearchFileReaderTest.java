package com.ens.alphabet_soup;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import java.io.IOException;

import org.junit.Before;
import org.junit.Test;

/**
 * Test WordSearchFileReader
 * 
 * @author ENS
 *
 */
public class WordSearchFileReaderTest {

	private String workingDir;

	/**
	 * Set up file location dir
	 */
	@Before
	public void init() {
		this.workingDir = "src/test/resources";
	}

	/**
	 * Read in example file
	 * 
	 * @throws Exception
	 */
	@Test
	public void readExampleFileTest() throws Exception {

		String pathAndFile = workingDir + "/puzzle_example.txt";
		WordSearchFileReader wordSearchFileReader = new WordSearchFileReader();
		PuzzleData puzzleData = wordSearchFileReader.load(pathAndFile);

		assertEquals(5, puzzleData.getNumRows());
		assertEquals(5, puzzleData.getNumCols());
		assertEquals(5, puzzleData.getPuzzleGrid().length);
		assertEquals('H', puzzleData.getPuzzleGrid()[0][0]);
		assertEquals('H', puzzleData.getPuzzleGrid()[1][4]);
		assertEquals(3, puzzleData.getSearchWordListToSortForBinarySearch().length);
		assertEquals(3, puzzleData.getSearchWordsInOrderMap().size());
	}

	/**
	 * Read in bigger example file
	 * 
	 * @throws Exception
	 */
	@Test
	public void readExampleBiggerFileTest() throws Exception {

		String pathAndFile = workingDir + "/puzzle_example_bigger.txt";
		WordSearchFileReader wordSearchFileReader = new WordSearchFileReader();
		PuzzleData puzzleData = wordSearchFileReader.load(pathAndFile);

		assertEquals(6, puzzleData.getNumRows());
		assertEquals(10, puzzleData.getNumCols());
		assertEquals(6, puzzleData.getPuzzleGrid().length);
		assertEquals('H', puzzleData.getPuzzleGrid()[0][0]);
		assertEquals('A', puzzleData.getPuzzleGrid()[0][1]);
		assertEquals('E', puzzleData.getPuzzleGrid()[1][1]);
		assertEquals(9, puzzleData.getSearchWordListToSortForBinarySearch().length);
		assertEquals(9, puzzleData.getSearchWordsInOrderMap().size());
	}

	/**
	 * Test for WordSearchInputException occurrence
	 * 
	 * @throws Exception
	 */
	@Test(expected = WordSearchInputException.class)
	public void readEmptyFileTest() throws Exception {

		String pathAndFile = "src/test/resources/puzzle_empty.txt";
		WordSearchFileReader wordSearchFileReader = new WordSearchFileReader();
		PuzzleData puzzleData = wordSearchFileReader.load(pathAndFile);
		assertNull(puzzleData);
	}

	/**
	 * Test for NumberFormatException occurrence
	 * 
	 * @throws Exception
	 */
	@Test(expected = NumberFormatException.class)
	public void readBadFirstLineFileTest() throws Exception {
		String pathAndFile = "src/test/resources/puzzle_bad_first_line.txt";
		WordSearchFileReader wordSearchFileReader = new WordSearchFileReader();
		PuzzleData puzzleData = wordSearchFileReader.load(pathAndFile);
		assertNull(puzzleData);
	}

	/**
	 * Test for IOException occurrence
	 * @throws Exception
	 */
	@Test(expected = IOException.class)
	public void readNoInputFileTest() throws Exception {
		String pathAndFile = "";
		WordSearchFileReader wordSearchFileReader = new WordSearchFileReader();
		PuzzleData puzzleData = wordSearchFileReader.load(pathAndFile);
		assertNull(puzzleData);
	}
}
