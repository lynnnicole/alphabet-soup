package com.ens.alphabet_soup;

import static org.junit.Assert.assertEquals;

import java.util.AbstractMap.SimpleEntry;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map.Entry;

import org.junit.Before;
import org.junit.Test;

/**
 * Test WordSearchSolutionFinder
 * 
 * @author ENS
 *
 */
public class WordSearchSolutionFinderTest {

	/**
	 * Test provided example
	 * 
	 * @throws Exception
	 */
	@Test
	public void solutionFinderExampleTest() throws Exception {
		// Setup puzzle data object
		PuzzleData puzzleData = new PuzzleData();
		puzzleData.setNumRows(5);
		puzzleData.setNumCols(5);

		List<String> puzzleLinesList = new ArrayList<>();
		puzzleLinesList.add("HASDF");
		puzzleLinesList.add("GEYBH");
		puzzleLinesList.add("JKLZX");
		puzzleLinesList.add("CVBLN");
		puzzleLinesList.add("GOODO");

		char[][] puzzleGrid = new char[puzzleData.getNumRows()][puzzleData.getNumCols()];
		Iterator<String> itr = puzzleLinesList.iterator();
		for (int row = 0; row < puzzleData.getNumRows(); row++) {
			String theLine = itr.next();
			puzzleGrid[row] = theLine.toCharArray();
		}

		puzzleData.setPuzzleGrid(puzzleGrid);

		puzzleData.getSearchWordsInOrderMap().put("HELLO", new SimpleEntry<>("HELLO", ""));
		puzzleData.getSearchWordsInOrderMap().put("GOOD", new SimpleEntry<>("GOOD", ""));
		puzzleData.getSearchWordsInOrderMap().put("BYE", new SimpleEntry<>("BYE", ""));

		// Tests
		WordSearchSolutionFinder wordSearchSolutionFinder = new WordSearchSolutionFinder();
		wordSearchSolutionFinder.performSearchForWords(puzzleData);

		Iterator<Entry<String, String>> orderedMapIterator = puzzleData.getSearchWordsInOrderMap().values().iterator();

		Entry<String, String> firstEntry = orderedMapIterator.next();

		assertEquals("HELLO", firstEntry.getKey());
		assertEquals("0:0 4:4", firstEntry.getValue());

		Entry<String, String> secondEntry = orderedMapIterator.next();

		assertEquals("GOOD", secondEntry.getKey());
		assertEquals("4:0 4:3", secondEntry.getValue());

		Entry<String, String> thirdEntry = orderedMapIterator.next();

		assertEquals("BYE", thirdEntry.getKey());
		assertEquals("1:3 1:1", thirdEntry.getValue());
	}

	/**
	 * Test bigger example
	 * 
	 * @throws Exception
	 */
	@Test
	public void solutionFinderBiggerExampleTest() throws Exception {
		// Setup puzzle data object
		PuzzleData puzzleData = new PuzzleData();
		puzzleData.setNumRows(6);
		puzzleData.setNumCols(10);

		List<String> puzzleLinesList = new ArrayList<>();
		puzzleLinesList.add("HASDFFOODD");
		puzzleLinesList.add("GEYBHYGIPH");
		puzzleLinesList.add("JKLZXFODDI");
		puzzleLinesList.add("CVBLNGPBPN");
		puzzleLinesList.add("GOODOGPDFT");
		puzzleLinesList.add("PHONEBOOKD");

		char[][] puzzleGrid = new char[puzzleData.getNumRows()][puzzleData.getNumCols()];
		Iterator<String> itr = puzzleLinesList.iterator();
		for (int row = 0; row < puzzleData.getNumRows(); row++) {
			String theLine = itr.next();
			puzzleGrid[row] = theLine.toCharArray();
		}

		puzzleData.setPuzzleGrid(puzzleGrid);

		puzzleData.getSearchWordsInOrderMap().put("HELLO", new SimpleEntry<>("HELLO", ""));
		puzzleData.getSearchWordsInOrderMap().put("GOOD", new SimpleEntry<>("GOOD", ""));
		puzzleData.getSearchWordsInOrderMap().put("BYE", new SimpleEntry<>("BYE", ""));
		puzzleData.getSearchWordsInOrderMap().put("HINT", new SimpleEntry<>("HINT", ""));
		puzzleData.getSearchWordsInOrderMap().put("PHONE", new SimpleEntry<>("PHONE", ""));
		puzzleData.getSearchWordsInOrderMap().put("BOY", new SimpleEntry<>("BOY", ""));
		puzzleData.getSearchWordsInOrderMap().put("FOOD", new SimpleEntry<>("FOOD", ""));
		// This is the way it gets inserted - remove spaces for key for finding, simple entry key remains as is for later output
		puzzleData.getSearchWordsInOrderMap().put("PHONEBOOK", new SimpleEntry<>("PHONE BOOK", ""));
		puzzleData.getSearchWordsInOrderMap().put("POGO", new SimpleEntry<>("POGO", ""));

		// Tests
		WordSearchSolutionFinder wordSearchSolutionFinder = new WordSearchSolutionFinder();
		wordSearchSolutionFinder.performSearchForWords(puzzleData);

		Iterator<Entry<String, String>> orderedMapIterator = puzzleData.getSearchWordsInOrderMap().values().iterator();

		// Ensure items are in same order as loaded
		Entry<String, String> firstEntry = orderedMapIterator.next();

		assertEquals("HELLO", firstEntry.getKey());
		assertEquals("0:0 4:4", firstEntry.getValue());

		Entry<String, String> secondEntry = orderedMapIterator.next();

		assertEquals("GOOD", secondEntry.getKey());
		assertEquals("4:0 4:3", secondEntry.getValue());

		Entry<String, String> thirdEntry = orderedMapIterator.next();

		assertEquals("BYE", thirdEntry.getKey());
		assertEquals("1:3 1:1", thirdEntry.getValue());

		Entry<String, String> FourthEntry = orderedMapIterator.next();

		assertEquals("HINT", FourthEntry.getKey());
		assertEquals("1:9 4:9", FourthEntry.getValue());

		Entry<String, String> fifthEntry = orderedMapIterator.next();

		assertEquals("PHONE", fifthEntry.getKey());
		assertEquals("5:0 5:4", fifthEntry.getValue());

		Entry<String, String> sixthEntry = orderedMapIterator.next();

		assertEquals("BOY", sixthEntry.getKey());
		assertEquals("3:7 1:5", sixthEntry.getValue());

		Entry<String, String> seventhEntry = orderedMapIterator.next();

		assertEquals("FOOD", seventhEntry.getKey());
		assertEquals("0:5 0:8", seventhEntry.getValue());

		Entry<String, String> eighthEntry = orderedMapIterator.next();

		// Make sure comes back to display with space
		assertEquals("PHONE BOOK", eighthEntry.getKey());
		assertEquals("5:0 5:8", eighthEntry.getValue());

		Entry<String, String> ninthEntry = orderedMapIterator.next();

		assertEquals("POGO", ninthEntry.getKey());
		assertEquals("3:6 0:6", ninthEntry.getValue());

	}

	/**
	 * Test to ensure name remains in search word output but that word is still found in puzzle
	 * 
	 * @throws Exception
	 */
	@Test
	public void solutionFinderSpaceInNameExampleTest() throws Exception {
		// Setup puzzle data object
		PuzzleData puzzleData = new PuzzleData();
		puzzleData.setNumRows(1);
		puzzleData.setNumCols(9);

		List<String> puzzleLinesList = new ArrayList<>();
		puzzleLinesList.add("KOOBENOHP");

		char[][] puzzleGrid = new char[puzzleData.getNumRows()][puzzleData.getNumCols()];
		Iterator<String> itr = puzzleLinesList.iterator();
		for (int row = 0; row < puzzleData.getNumRows(); row++) {
			String theLine = itr.next();
			puzzleGrid[row] = theLine.toCharArray();
		}

		puzzleData.setPuzzleGrid(puzzleGrid);

		puzzleData.getSearchWordsInOrderMap().put("PHONEBOOK", new SimpleEntry<>("PHONE BOOK", ""));

		// Tests
		WordSearchSolutionFinder wordSearchSolutionFinder = new WordSearchSolutionFinder();
		wordSearchSolutionFinder.performSearchForWords(puzzleData);

		Iterator<Entry<String, String>> orderedMapIterator = puzzleData.getSearchWordsInOrderMap().values().iterator();

		// Ensure items are in same order as loaded
		Entry<String, String> firstEntry = orderedMapIterator.next();

		assertEquals("PHONE BOOK", firstEntry.getKey());
		assertEquals("0:8 0:0", firstEntry.getValue());
	}

	/**
	 * Test to ensure word list prints propertly, but no indices are returned for words not found 
	 * @throws Exception
	 */
	@Test
	public void solutionFinderNotFoundExampleTest() throws Exception {
		// Setup puzzle data object
		PuzzleData puzzleData = new PuzzleData();
		puzzleData.setNumRows(1);
		puzzleData.setNumCols(10);

		List<String> puzzleLinesList = new ArrayList<>();
		puzzleLinesList.add("HELLOTHERE");

		char[][] puzzleGrid = new char[puzzleData.getNumRows()][puzzleData.getNumCols()];
		Iterator<String> itr = puzzleLinesList.iterator();
		for (int row = 0; row < puzzleData.getNumRows(); row++) {
			String theLine = itr.next();
			puzzleGrid[row] = theLine.toCharArray();
		}

		puzzleData.setPuzzleGrid(puzzleGrid);

		puzzleData.getSearchWordsInOrderMap().put("DESK", new SimpleEntry<>("DESK", ""));

		// Tests
		WordSearchSolutionFinder wordSearchSolutionFinder = new WordSearchSolutionFinder();
		wordSearchSolutionFinder.performSearchForWords(puzzleData);

		Iterator<Entry<String, String>> orderedMapIterator = puzzleData.getSearchWordsInOrderMap().values().iterator();

		// Ensure items are in same order as loaded
		Entry<String, String> firstEntry = orderedMapIterator.next();

		assertEquals("DESK", firstEntry.getKey());
		assertEquals("", firstEntry.getValue());
	}
}
