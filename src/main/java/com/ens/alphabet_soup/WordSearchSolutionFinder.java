package com.ens.alphabet_soup;

import java.util.Arrays;
import java.util.Map.Entry;

/**
 * Main logic for the iteration through the word search characters to find the matches and their indices
 * 
 * @author ENS
 *
 */
public class WordSearchSolutionFinder {

	/**
	 * Sets up variables for working through the puzzle size and shape for coordinates and differences (direction of next characters) that
	 * make sense, and calls logic use this info to iterate over the character sequences
	 * 
	 * @param puzzleData
	 */
	protected void performSearchForWords(PuzzleData puzzleData) {
		int totalNumMatchesFound = 0;

		int numRows = puzzleData.getNumRows();
		int numCols = puzzleData.getNumCols();

		// Iterate each row
		for (int row = 0; row < numRows; row++) {
			// Iterate each column
			for (int col = 0; col < numCols; col++) {
				// Iterate 1 char each row
				for (int rowDiff = -1; rowDiff <= 1; rowDiff++) {
					// Iterate 1 char each column
					for (int colDiff = -1; colDiff <= 1; colDiff++) {
						// Work through combinations that make sense
						int rowPlusRowDiff = row + rowDiff;
						int colPlusColDiff = col + colDiff;
						if ((rowDiff != 0 || colDiff != 0) && (rowPlusRowDiff >= 0) && (colPlusColDiff >= 0) && (rowPlusRowDiff < numRows)
						        && (colPlusColDiff < numCols)) {
							// Calls the logic for checking for matches for each of these combos
							totalNumMatchesFound += checkForMatchingWord(puzzleData, row, col, rowDiff, colDiff);
							if (totalNumMatchesFound == puzzleData.getSearchWordListToSortForBinarySearch().length) {
								// All words have been found, no sense looking further
								break;
							}
						}
					}
				}
			}
		}
	}

	/**
	 * Checks to see if character sequence (chars at location specified and location using diff coordinates) is in the list of search words
	 * provided using binary search.
	 * 
	 * Search word list is sorted so binary search returns either the index of the search key (if a match), or a negative value by which one
	 * can calculate where the insertion point would be for the word in the list.
	 * 
	 * If match, print out coordinates and return.
	 * 
	 * If not a match, keeps iterating using diff coordinates which 'continue in same direction' to append characters to character sequence
	 * being checked to see if it eventually matches a search word.
	 * 
	 * While doing so, continues to check for match, or possible match of beginning of word until character sequence being checked either
	 * falls outside the list array elements for its location of insert (no longer falls within word list 'range') or no longer matches word
	 * list element at the insert point calculated (thus failing to match the word).
	 * 
	 * @param puzzleData
	 * @param row
	 * @param col
	 * @param rowDiff
	 * @param colDiff
	 * @return
	 */
	private int checkForMatchingWord(PuzzleData puzzleData, int row, int col, int rowDiff, int colDiff) {

		int numMatchesFound = 0;
		int arrayPositionInDictionary = 0;
		StringBuilder charSeq = new StringBuilder();

		charSeq.append(puzzleData.getPuzzleGrid()[row][col]);

		for (int i = row + rowDiff, j = col + colDiff; i >= 0 && j >= 0 && i < puzzleData.getNumRows()
		        && j < puzzleData.getNumCols(); i += rowDiff, j += colDiff) {
			// This builds up the character sequence we're checking for a match in the list for
			charSeq.append(puzzleData.getPuzzleGrid()[i][j]);

			// Perform the binary search check to see if match or where current charSeq would fall in list
			int indexOrInsertion = Arrays.binarySearch(puzzleData.getSearchWordListToSortForBinarySearch(), charSeq.toString());
			if (indexOrInsertion >= 0) {
				// Current charSeq has been found in dictionary list - it's a match!

				// Get the entry from the ordered map to update with the indices
				Entry<String, String> entryToUpdate = puzzleData.getSearchWordsInOrderMap().get(charSeq.toString());
				// Store starting and ending indices on corresponding entry
				entryToUpdate.setValue(row + ":" + col + " " + i + ":" + j);

				// Update entry that now has indices in the map
				puzzleData.getSearchWordsInOrderMap().put(charSeq.toString(), entryToUpdate);

				// Increment the number of matches found so can stop when all are located
				numMatchesFound++;
			} else {
				// The charSeq not found in array, adjust value to see where would be in list
				arrayPositionInDictionary = (-indexOrInsertion - 1);
				// Test to see if this character combo could still possibly be in list
				if (puzzleData.getSearchWordListToSortForBinarySearch().length == arrayPositionInDictionary
				        || !(puzzleData.getSearchWordListToSortForBinarySearch()[arrayPositionInDictionary])
				                .startsWith(charSeq.toString())) {
					// Word falls outside the dictionary entries or
					// Doesn't match the starting of the word in that location
					break;
				}
			}
		}
		return numMatchesFound;
	}
}
