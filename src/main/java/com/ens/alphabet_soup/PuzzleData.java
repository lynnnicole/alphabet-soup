package com.ens.alphabet_soup;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

/**
 * Puzzle data to store model for use with processing logic
 * 
 * @author ENS
 *
 */
public class PuzzleData {
	// From file
	private int numRows;

	// From file
	private int numCols;

	private char[][] puzzleGrid;
	private String[] searchWordListToSortForBinarySearch;

	// From File
	List<String> puzzleLinesList = new ArrayList<>();

	private Map<String, Entry<String, String>> searchWordsInOrderMap = new LinkedHashMap<>();

	/**
	 * @return the numRows
	 */
	public int getNumRows() {
		return numRows;
	}

	/**
	 * @param numRows the numRows to set
	 */
	public void setNumRows(int numRows) {
		this.numRows = numRows;
	}

	/**
	 * @return the numCols
	 */
	public int getNumCols() {
		return numCols;
	}

	/**
	 * @param numCols the numCols to set
	 */
	public void setNumCols(int numCols) {
		this.numCols = numCols;
	}

	/**
	 * @return the puzzleGrid
	 */
	public char[][] getPuzzleGrid() {
		if (puzzleGrid == null) {
			generatePuzzleGrid();
		}
		return puzzleGrid;
	}

	/**
	 * @param puzzleGrid the puzzleGrid to set
	 */
	public void setPuzzleGrid(char[][] puzzleGrid) {
		this.puzzleGrid = puzzleGrid;
	}

	/**
	 * @return the searchWordListToSortForBinarySearch
	 */
	public String[] getSearchWordListToSortForBinarySearch() {
		if (searchWordListToSortForBinarySearch == null) {
			sortListForBinarySearch();
		}
		return searchWordListToSortForBinarySearch;
	}

	/**
	 * @return the puzzleLinesList
	 */
	public List<String> getPuzzleLinesList() {
		return puzzleLinesList;
	}

	/**
	 * @param puzzleLinesList the puzzleLinesList to set
	 */
	public void setPuzzleLinesList(List<String> puzzleLinesList) {
		this.puzzleLinesList = puzzleLinesList;
	}

	/**
	 * @param searchWordListToSortForBinarySearch the searchWordListToSortForBinarySearch to set
	 */
	public void setSearchWordListToSortForBinarySearch(String[] searchWordListToSortForBinarySearch) {
		this.searchWordListToSortForBinarySearch = searchWordListToSortForBinarySearch;
	}

	/**
	 * @return the searchWordsInOrderMap
	 */
	public Map<String, Entry<String, String>> getSearchWordsInOrderMap() {
		return searchWordsInOrderMap;
	}

	/**
	 * @param searchWordsInOrderMap the searchWordsInOrderMap to set
	 */
	public void setSearchWordsInOrderMap(Map<String, Entry<String, String>> searchWordsInOrderMap) {
		this.searchWordsInOrderMap = searchWordsInOrderMap;
	}

	/**
	 * Sorts the list in order for binary search
	 * 
	 */
	private void sortListForBinarySearch() {
		// Take the map that was created and grab the keys to a list for binary search
		searchWordListToSortForBinarySearch = searchWordsInOrderMap.keySet().toArray(new String[0]);
		// Want array in sorted order because using binary sort function on it later
		Arrays.sort(searchWordListToSortForBinarySearch);
	}

	/**
	 * Generates the puzzle grid for use by the processing logic based on whats been set
	 */
	private void generatePuzzleGrid() {
		// Build up the puzzle character array
		puzzleGrid = new char[numRows][numCols];
		Iterator<String> itr = puzzleLinesList.iterator();
		for (int row = 0; row < numRows; row++) {
			String theLine = itr.next();
			puzzleGrid[row] = theLine.toCharArray();
		}
	}

	@Override
	public String toString() {
		return "PuzzleData [numRows=" + numRows + ", numCols=" + numCols + ", puzzleGrid=" + Arrays.toString(puzzleGrid)
		        + ", searchWordListToSortForBinarySearch=" + Arrays.toString(searchWordListToSortForBinarySearch) + ", puzzleLinesList="
		        + puzzleLinesList + ", searchWordsInOrderMap=" + searchWordsInOrderMap + "]";
	}
}
