package com.ens.alphabet_soup;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Provide an answer key to a word search. The word search is a traditional game consisting of a grid of characters in which a selection of
 * words have been hidden. Provided with the list of words that have been hidden, find the words within the grid of characters.
 * 
 * Output the list of words using the following format, designating the word and the indices of the starting and ending letters of the word.
 * 
 * WORD #:# #:#
 * 
 * Example:
 * 
 * HELLO 0:0 4:4
 * 
 * Assumptions:
 * 
 * - This is a legit word search where the word to find would not appear in multiple locations on the word search grid.
 * 
 * - Any words in the search list that have a space in there are to be found as if no space exists as that is how they appear in puzzles.
 * 
 * - Input file is consistent with its use of casing.
 * 
 * @author ENS
 *
 */
public class WordSearchApp {

	private static final Logger LOG = LogManager.getLogger(WordSearchApp.class);

	/**
	 * Main method
	 * 
	 * Instantiates WordSearch and calls solve puzzle
	 * 
	 * @param args
	 */
	public static void main(String[] args) {

		try {
			// Get the puzzle data loaded
			PuzzleData puzzleDataModel = loadPuzzleDataFromFile();

			// Create the view to display the output
			WordSearchView wordSearchView = new WordSearchView();

			// Set up the controller
			WordSearchController wordSearchController = new WordSearchController(puzzleDataModel, wordSearchView);

			// Find the solutions
			wordSearchController.performWordSearch();

			// Display the solution
			wordSearchController.displayWordSearchSolutions();
		} catch (Exception e) {
			LOG.error("Word Search App Error: {}", e.getMessage());
		}
	}

	/**
	 * Loads the data from the file into the PuzzleData object
	 * 
	 * @return
	 * @throws WordSearchInputException
	 */
	private static PuzzleData loadPuzzleDataFromFile() throws IOException, WordSearchInputException {
		PuzzleData puzzleData = null;
		WordSearchFileReader wordSearchFileReader = null;
		LOG.info("{}", listPuzzleFormatInstructions());

		// Kick off request for input and loading of file data
		wordSearchFileReader = new WordSearchFileReader();

		String fileName = "";
		LOG.info("Specify the path and filename: ");
		try (BufferedReader in = new BufferedReader(new InputStreamReader(System.in))) {
			fileName = in.readLine();
			if (fileName.isEmpty()) {
				throw new WordSearchInputException(
				        "File path and name must be specified. Rerun the program and specify a valid path and filename.");
			}
			puzzleData = wordSearchFileReader.load(fileName);
		}

		return puzzleData;
	}

	/**
	 * Builds up puzzle format instructions to display to help when there are issues
	 * 
	 * @return
	 */
	private static StringBuilder listPuzzleFormatInstructions() {
		StringBuilder sb = new StringBuilder();
		sb.append(
		        "============================================ WORD SEARCH SOLUTION FINDER ============================================\n");
		sb.append("The program accepts an ASCII text file file as input.\n\n");
		sb.append("The file contains the word search board along with the words that need to be found in three parts:\n");
		sb.append("\t - First line: The number of rows and columns that make up the word search grid, separated by an 'x'.\n");
		sb.append("\t - Second section: The word search character grid. Each character will separated by a space.\n");
		sb.append("\t - Third section: The words to be found.\n\n");
		sb.append("Example of input file format:\n\n");
		sb.append("5x5\n");
		sb.append("H A S D F\n");
		sb.append("G E Y B H\n");
		sb.append("J K L Z X\n");
		sb.append("C V B L N\n");
		sb.append("G O O D O\n");
		sb.append("HELLO\n");
		sb.append("GOOD\n");
		sb.append("BYE\n");
		sb.append(
		        "=====================================================================================================================\n");		

		return sb;
	}
}
