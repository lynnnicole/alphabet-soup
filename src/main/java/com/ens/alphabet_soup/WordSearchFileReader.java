package com.ens.alphabet_soup;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.AbstractMap.SimpleEntry;

/**
 * WordSearch file reader
 *
 * The input file is an ASCII text file containing the word search board along with the words that need to be found.
 * 
 * The file contains three parts. The first part is the first line, and specifies the number of rows and columns in the grid of characters,
 * separated by an 'x'. The second part provides the grid of characters in the word search. The third part in the file specifies the words
 * to be found.
 * 
 * The first line indicates how many following lines in the file contain the rows of characters that make up the word search grid. Each row
 * in the word search grid will have the specified number of columns of characters, each separated with a space. The remaining lines in the
 * file specify the words to be found.
 * 
 * Reads in the file and sets the puzzle data so it can be used by the match checker
 * 
 * @author ENS
 *
 */
public class WordSearchFileReader {

	private static final String NUM_ROW_AND_COL_DIVIDER = "x";
	private static final String SINGLE_SPACE = " ";

	/**
	 * Reads in the passed in file and loads the puzzle data for use by the match checker
	 * 
	 * @param pathAndFile
	 * @return
	 * @throws IOException
	 * @throws WordSearchInputException
	 */
	protected PuzzleData load(String pathAndFile) throws IOException, WordSearchInputException {
		PuzzleData puzzleData = new PuzzleData();
		try (BufferedReader puzzleStreamReader = new BufferedReader(new FileReader(pathAndFile))) {
			int lineCounter = 0;
			String eachLine;

			while ((eachLine = puzzleStreamReader.readLine()) != null) {
				if (lineCounter == 0) {
					// Set Rows and Columns
					int dividerBetweenNumIndex = eachLine.indexOf(NUM_ROW_AND_COL_DIVIDER);
					puzzleData.setNumRows(parseDimensionNum(eachLine, 0, dividerBetweenNumIndex));
					puzzleData.setNumCols(parseDimensionNum(eachLine, dividerBetweenNumIndex + 1));
				} else if (lineCounter <= puzzleData.getNumRows()) {
					// Set puzzle lines, removing spaces
					puzzleData.getPuzzleLinesList().add(eachLine.replace(SINGLE_SPACE, ""));
				} else {
					// Create search word list with entry for retained order and spaces to display once finished
					// Remove any spaces for key/what to find and just treat as one word per req specifications
					puzzleData.getSearchWordsInOrderMap().put(eachLine.replace(SINGLE_SPACE, ""), new SimpleEntry<>(eachLine, ""));
				}
				lineCounter++;
			}

			// Counter still 0, no lines in file, display error and instructions
			if (lineCounter == 0) {
				throw new WordSearchInputException("Puzzle file is empty: " + pathAndFile);
			}
		}

		return puzzleData;
	}

	/**
	 * Gets the puzzle dimension from the first input line
	 * 
	 * @param eachLine
	 * @param beginIndex
	 * @param endIndex
	 * @return
	 */
	private int parseDimensionNum(String eachLine, int beginIndex, int... endIndex) {
		// First row defines the puzzle number of row and cols
		int numParsed = 0;
		if (endIndex != null && endIndex.length > 0) {
			numParsed = Integer.parseInt(eachLine.substring(beginIndex, endIndex[0]));
		} else {
			numParsed = Integer.parseInt(eachLine.substring(beginIndex));
		}
		return numParsed;
	}
}
