package com.ens.alphabet_soup;

/**
 * Exceptions for the Word Search App loading file process
 * 
 * @author ENS
 *
 */
public class WordSearchInputException extends Exception {

	private static final long serialVersionUID = 1L;

	public WordSearchInputException(String errorMessage) {
		super(errorMessage);
	}
}
