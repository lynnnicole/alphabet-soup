package com.ens.alphabet_soup;

/**
 * Controller for the WordSearch logic
 * 
 * @author ENS
 *
 */
public class WordSearchController {

	private PuzzleData puzzleDataModel;
	private WordSearchView wordSearchView;

	/**
	 * Constructor for setting the model and the view
	 * 
	 * @param model
	 * @param view
	 */
	public WordSearchController(PuzzleData model, WordSearchView view) {
		this.puzzleDataModel = model;
		this.wordSearchView = view;
	}

	/**
	 * Perform the core logic of finding the words in the puzzle grid
	 * 
	 */
	public void performWordSearch() {
		WordSearchSolutionFinder wordSearchMatchChecker = new WordSearchSolutionFinder();
		wordSearchMatchChecker.performSearchForWords(puzzleDataModel);
	}

	/**
	 * Display the solutions
	 */
	public void displayWordSearchSolutions() {
		wordSearchView.printWordSearchWordsAndIndices(puzzleDataModel.getSearchWordsInOrderMap());
	}
}
