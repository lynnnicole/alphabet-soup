package com.ens.alphabet_soup;

import java.util.Map;
import java.util.Map.Entry;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Output the search word list and their start and end indices to the user in the console
 * 
 * @author ENS
 *
 */
public class WordSearchView {

	private static final Logger LOG = LogManager.getLogger(WordSearchView.class);
	
	/**
	 * Prints out the search words and their indices 
	 * 
	 * @param searchWordsInOrderMap
	 */
	public void printWordSearchWordsAndIndices(Map<String, Entry<String, String>> searchWordsInOrderMap) {

		for (Map.Entry<String, String> entry : searchWordsInOrderMap.values()) {
			LOG.info("{} {}", entry.getKey(), entry.getValue());
		}
	}
}
